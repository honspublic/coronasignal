
# Corona Signal
![Swift](https://img.shields.io/badge/swift-5.3-orange.svg?style=for-the-badge)
![SwiftUI](https://img.shields.io/badge/use-swift_ui-orange.svg?style=for-the-badge)
![Combine](https://img.shields.io/badge/use-combine-orange.svg?style=for-the-badge)
![Xcode](https://img.shields.io/badge/Xcode-12-blue.svg?style=for-the-badge)
![license](https://img.shields.io/github/license/sgr-ksmt/FireTodo.svg?style=for-the-badge)

Simple Corona Monitor using **SwiftUI**/**Combine**.

## Feature

- Live update (every 10 min) of the corona situation for the users current location
- Possibility to switch from location tracking to an arbitrary location on the map (useful if you're outside germany, but you wanna track the state of your homelocation)
- Push notification on state change

## Setup an Run

- Clone this repository.
- Run application!

### Outside Germany
If your not in germany you can disable the location tracing in favour of the location discovery mode by clicking on the target icon at bottom right of the map. This mode unfixes the map so that the user can choose the location tracing point on the map.

## Dependencies

- [SwiftUI](https://developer.apple.com/xcode/swiftui/)
- [Combine](https://developer.apple.com/documentation/combine)
- [RKI Corona API]([https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/917fc37a709542548cc3be077a786c17_0](https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/917fc37a709542548cc3be077a786c17_0))

## Todo

- [ ] Support landscape mode
- [ ] Add initial tour to explain features
- [ ] Improve error handling (when your location is not in germany or there is no data for your location)
- [ ] Automatically switch to location discovery mode if the user denies the authorization for location updates
- [ ] RKI does offer a lot of information not just the incidence that we use currently. Would be nice to show a lot more on the app.
- [ ] Translate into more languages
- [ ] Extend to macOS

## Communication

- If you found a bug, open an issue.
- If you have a feature request, open an issue.
- If you want to contribute, submit a pull request.:muscle:

## License

**CoronaSignal** is under MIT license. See the [LICENSE](LICENSE) file for more info.