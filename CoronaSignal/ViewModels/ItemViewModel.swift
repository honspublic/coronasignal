//
//  ItemsViewModel.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import Combine
import Foundation
import CoreLocation

let defaultCenterCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)

class ItemViewModel: ObservableObject, Identifiable {
    var subscriptions: Set<AnyCancellable> = []
    @Published var centerCoordinate: CLLocationCoordinate2D
    @Published var followUser: Bool = true
    @Published var loading: Bool = true
    @Published var error: CoronaStateError?
    @Published var item: Item?
    var dirty: Bool = false
    var state: CoronaState { item?.state ?? .unknown }
    var areas: [[CLLocationCoordinate2D]]? {
        return item?.features.first?.geometry.rings.compactMap({ (ring: Ring) -> [CLLocationCoordinate2D]? in
            ring.points.compactMap({ (points: [Double]) -> CLLocationCoordinate2D? in
                guard points.count >= 2 else { return nil }
                return CLLocationCoordinate2DMake(points[1], points[0])
            })
        })
    }
    
    init(centerCoordinate: CLLocationCoordinate2D = defaultCenterCoordinate) {
        self.centerCoordinate = centerCoordinate
    }
    
    func reload() {
        CoronaService
            .loadItem(geometry: [centerCoordinate.longitude, centerCoordinate.latitude])?
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] value in
                guard let self = self else { return }
                if case let .failure(error) = value {
                    self.item = nil
                    self.error = error
                } else {
                    self.error = nil
                }
                self.loading = false
            }, receiveValue: { [weak self] item in
                guard let self = self else { return }
                self.dirty = self.state != item.state
                self.item = item
            })
            .store(in: &subscriptions)
    }
}
