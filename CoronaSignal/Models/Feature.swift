//
//  Feature.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 20/11/20.
//

import Foundation

struct Feature: Codable, Hashable {
    let attributes: Attributes
    let geometry: Geometry
    
    private enum CodingKeys : String, CodingKey {
        case attributes
        case geometry
    }
}
