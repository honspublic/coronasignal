//
//  CoronaState.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 25/11/20.
//

import Foundation
import SwiftUI

enum CoronaState: String {
    case unknown = ""
    case green = "green"
    case yellow = "yellow"
    case red = "red"
    case darkred = "darkred"
}

extension CoronaState {
    var color: Color {
        switch self {
        case .green:
            return Color.green
        case .yellow:
            return Color.yellow
        case .red:
            return Color.red.lighter()
        case .darkred:
            return Color.red.darker()
        default:
            return Color.clear
        }
    }
}
