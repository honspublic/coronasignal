//
//  Geometry.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 01/12/20.
//

import Foundation

struct Geometry: Codable, Hashable {
    let rings: [Ring]
    
    private enum CodingKeys : String, CodingKey {
        case rings
    }
}
