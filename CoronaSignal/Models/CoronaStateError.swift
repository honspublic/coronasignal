//
//  CoronaStateError.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import Foundation

enum CoronaStateError: Error {
  case message(String)
  case other(Error)

  static func map(_ error: Error) -> CoronaStateError {
    return (error as? CoronaStateError) ?? .other(error)
  }
}

extension CoronaStateError: CustomStringConvertible {
  var description: String {
    switch self {
    case .message(let message):
      return message
    case .other(let error):
      return error.localizedDescription
    }
  }
}
