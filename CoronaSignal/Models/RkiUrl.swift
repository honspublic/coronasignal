//
//  RkiUrl.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 23/11/20.
//

import Foundation

let kQueryF = "f"
let kQueryGeometry = "geometry"
let kQueryGeometryType = "geometryType"
let kQueryInSR = "inSR"
let kQueryOutFields = "outFields"
let kQueryOutSR = "outSR"
let kQueryReturnDistinctValues = "returnDistinctValues"
let kQueryReturnGeometry = "returnGeometry"
let kQuerySpatialRel = "spatialRel"
let kQueryWhere = "where"

struct RkiUrl {
    var baseUrl = "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_Landkreisdaten/FeatureServer/0/query"
    var query: [String: String?] { [
        kQueryF: f,
        kQueryGeometry: geometry.map({ String(format: "%.3f", $0) }).joined(separator: ","),
        kQueryGeometryType: geometryType,
        kQueryOutFields: outFields.joined(separator: ","),
        kQueryInSR: inSR,
        kQuerySpatialRel: spatialRel,
        kQueryReturnGeometry: returnGeometry.description,
        kQueryReturnDistinctValues: returnDistinctValues.description,
        kQueryOutSR: outSR,
        kQueryWhere: whereClause,
    ] }
    let f: String = "json"
    let geometry: [Double]
    let geometryType: String = "esriGeometryPoint"
    let inSR: String = "4326"
    let outFields: [String]
    let outSR: String = "4326"
    let returnGeometry: Bool = true
    let returnDistinctValues: Bool = false
    let spatialRel: String = "esriSpatialRelIntersects"
    let whereClause: String = "1=1"
    var url: URL? {
        var urlComponents = URLComponents(string: baseUrl)
        let queryItems = query.compactMap({ URLQueryItem(name: $0.key, value: $0.value) })
        urlComponents?.queryItems = queryItems
        return urlComponents?.url
    }
    
    init(geometry:[Double], outFields: [String] = ["last_update", "county", "BL", "cases7_bl_per_100k"]) {
        self.geometry = geometry
        self.outFields = outFields
    }
}
