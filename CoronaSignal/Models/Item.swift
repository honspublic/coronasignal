//
//  Item.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import Foundation

struct Item: Codable, Hashable {
    let objectIdFieldName: String
    //    let uniqueIdField: UniqueId
    let globalIdFieldName: String
    let geometryType: String
    //    let spatialReference: SpatialReference
    let features: [Feature]
    
    private enum CodingKeys : String, CodingKey {
        case objectIdFieldName
        case globalIdFieldName
        case geometryType
        case features
    }
}
