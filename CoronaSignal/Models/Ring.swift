//
//  Ring.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 01/12/20.
//

import Foundation

struct Ring: Codable, Hashable {
    let points: [[Double]]
    
    init(from decoder: Decoder) throws {
        self.points = try decoder.singleValueContainer().decode([[Double]].self)
    }
}
