//
//  Item+Convenience.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 29/11/20.
//

import Foundation

extension Item {
    var county: String? { features.first?.attributes.county }
    var bl: String? { features.first?.attributes.bl }
    var last_update: String? { features.first?.attributes.last_update }
    var cases7_bl_per_100k: Double? { features.first?.attributes.cases7_bl_per_100k }
    var state: CoronaState {
        guard let firstItem = features.first else {
            return .unknown
        }
        switch firstItem.attributes.cases7_bl_per_100k {
        case 0..<35:
            return .green
        case 35..<50:
            return .yellow
        case 50..<100:
            return .red
        default:
            return .darkred
        }
    }
}
