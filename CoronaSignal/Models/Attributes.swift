//
//  Attributes.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 20/11/20.
//

import Foundation

struct Attributes: Codable, Hashable {
    let last_update: String
    let county: String
    let bl: String
    let cases7_bl_per_100k: Double
    
    private enum CodingKeys : String, CodingKey {
        case last_update
        case county
        case bl = "BL"
        case cases7_bl_per_100k
    }
}
