//
//  Color+Shade.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 25/11/20.
//

import SwiftUI

extension Color {
    public func lighter(by amount: CGFloat = 0.2) -> Self { Self(UIColor(self).lighter(by: amount)) }
    public func darker(by amount: CGFloat = 0.2) -> Self { Self(UIColor(self).darker(by: amount)) }
}
