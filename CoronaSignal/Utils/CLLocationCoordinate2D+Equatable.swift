//
//  CLLocationCoordinate2D+Equatable.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 25/11/20.
//

import CoreLocation

extension CLLocationCoordinate2D: Equatable {
    static public func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}
