//
//  CoronaSignalApp.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import SwiftUI

@main
struct CoronaSignalApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
