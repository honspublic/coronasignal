//
//  LocalNotificationService.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 25/11/20.
//

import Foundation
import UserNotifications
import SwiftUI
import os

class LocalNotificationService: ObservableObject {
    
    var notifications = [Notification]()
    
    init() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
            if granted == true && error == nil {
                os_log(.debug, "Allowed")
            } else {
                os_log(.debug, "Error")
            }
        }
    }
    
    func sendNotification(title: String, subtitle: String? = nil, body: String, imageName: String = "logo", launchIn: Double = 1.0) {
        let content = UNMutableNotificationContent()
        content.title = title
        if let subtitle = subtitle {
            content.subtitle = subtitle
        }
        content.body = body
        
        guard let imageURL = Bundle.main.url(forResource: imageName, withExtension: "png") else {
            return
        }
        do {
            let attachment = try UNNotificationAttachment(identifier: imageName, url: imageURL, options: .none)
            content.attachments = [attachment]
        } catch {
            os_log(.error, "Error \(error.localizedDescription)")
        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: launchIn, repeats: false)
        let request = UNNotificationRequest(identifier: "CoronaState", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}
