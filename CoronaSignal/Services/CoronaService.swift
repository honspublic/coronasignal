//
//  CoronaService.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import Combine
import Foundation
import MapKit

final class CoronaService {
  static func loadItem(geometry: [Double]) -> AnyPublisher<Item, CoronaStateError>? {
    guard let url = RkiUrl(geometry: geometry).url else {
        return nil
    }
    debugPrint("URL: \(url.absoluteString)")
    return URLSession.shared.dataTaskPublisher(for: url)
      .tryMap { response in
        if let httpURLResponse = response.response as? HTTPURLResponse,
              !(200...299 ~= httpURLResponse.statusCode) {
          throw CoronaStateError.message("Got an HTTP \(httpURLResponse.statusCode) error.")
        }
        return response.data
      }
      .decode(type: Item.self, decoder: JSONDecoder())
      .mapError { CoronaStateError.map($0) }
      .eraseToAnyPublisher()
  }
}
