//
//  TrafficLightView.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import SwiftUI

struct TrafficLightView: View {
    var coronaState: CoronaState = .unknown
    var selectedState: CoronaState = .unknown
    
    var body: some View {
        Circle()
            .fill(coronaState.color)
            .frame(width: 50, height: 50)
            .opacity(coronaState == selectedState ? 1.0 : 0.1)
    }
}

struct TrafficLightView_Previews: PreviewProvider {
    static var previews: some View {
        TrafficLightView()
    }
}
