//
//  MapView.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import SwiftUI
import MapKit

// MARK: Struct that handle the map
struct MapView: UIViewRepresentable {
    @Binding var centerCoordinate: CLLocationCoordinate2D
    @Binding var showMapAlert: Bool
    @Binding var followUser: Bool
    var areas: [[CLLocationCoordinate2D]]?
    var color: Color
    let locationManager = CLLocationManager()
    
    ///Creating map view at startup
    func makeUIView(context: Context) -> MKMapView {
        locationManager.delegate = context.coordinator
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        let map = MKMapView()
        map.delegate = context.coordinator
        map.isRotateEnabled = false
        map.showsUserLocation = false
        map.userTrackingMode = .follow
        return map
    }
    
    func updateUIView(_ view: MKMapView, context: Context) {
        view.removeOverlays(view.overlays)
        
        areas?.forEach({ (coordinates: [CLLocationCoordinate2D]) in
            let polygon = MKColoredPolygon(coordinates: coordinates, count: coordinates.count)
            polygon.fillColor = UIColor(color)
            view.addOverlay(polygon)
        })
        
        view.removeAnnotations(view.annotations)
        if followUser {
            view.isUserInteractionEnabled = false
            view.showsUserLocation = true
        } else {
            let annotation = MKPointAnnotation()
            annotation.coordinate = centerCoordinate
            view.addAnnotation(annotation)
            
            view.isUserInteractionEnabled = true
            view.showsUserLocation = false
        }
    }
    
    ///Use class Coordinator method
    func makeCoordinator() -> MapView.Coordinator {
        return Coordinator(self)
    }
    
    //MARK: - Core Location manager delegate
    class Coordinator: NSObject, CLLocationManagerDelegate, MKMapViewDelegate {
        
        var parent: MapView
        
        init(_ parent: MapView) {
            self.parent = parent
        }
        
        func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
            if parent.followUser {
                parent.centerCoordinate = userLocation.coordinate
                mapView.setCenter(userLocation.coordinate, animated: true)
            }
        }
        
        func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
            if !mapView.showsUserLocation {
                parent.centerCoordinate = mapView.centerCoordinate
            }
        }
        
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            if annotation is MKUserLocation {
                return nil;
            } else {
                let pinIdent = "Pin";
                var pinView: MKAnnotationView?
                if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) {
                    dequeuedView.annotation = annotation;
                    pinView = dequeuedView;
                } else {
                    pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: pinIdent);
                    pinView?.image = UIImage(named: "pin")
                }
                return pinView;
            }
        }
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            if overlay is MKPolyline {
                let polylineRenderer = MKPolylineRenderer(overlay: overlay)
                polylineRenderer.strokeColor = .red
                polylineRenderer.lineWidth = 1
                return polylineRenderer
            } else if overlay is MKPolygon {
                let polygonView = MKPolygonRenderer(overlay: overlay)
                if polygonView.contains(coordinate: mapView.centerCoordinate) {
                    polygonView.lineWidth = 1
                    polygonView.strokeColor = (overlay as? MKColoredPolygon)?.fillColor ?? UIColor.red
                    polygonView.fillColor = polygonView.strokeColor?.withAlphaComponent(0.3)
                }
                return polygonView
            }
            return MKPolylineRenderer(overlay: overlay)
        }
        
        ///Switch between user location status
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            switch status {
            case .restricted:
                break
            case .denied:
                parent.showMapAlert.toggle()
                return
            case .notDetermined:
                parent.locationManager.requestWhenInUseAuthorization()
                return
            case .authorizedWhenInUse:
                parent.locationManager.requestAlwaysAuthorization()
                return
            case .authorizedAlways:
                parent.locationManager.allowsBackgroundLocationUpdates = true
                parent.locationManager.pausesLocationUpdatesAutomatically = false
                return
            @unknown default:
                break
            }
        }
    }
}

class MKColoredPolygon: MKPolygon {
    var fillColor: UIColor?
}

extension MKPolygonRenderer {
    func contains(coordinate: CLLocationCoordinate2D) -> Bool {
        let currentMapPoint: MKMapPoint = MKMapPoint(coordinate)
        let polygonViewPoint: CGPoint = point(for: currentMapPoint)
        guard path != nil else { return false }
        return path.contains(polygonViewPoint)
    }
}
