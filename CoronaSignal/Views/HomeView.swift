//
//  HomeView.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import SwiftUI
import MapKit

struct HomeView: View {
    @ObservedObject var viewModel: ItemViewModel
    @State var showMapAlert = false
    
    var states: [CoronaState] = [.green, .yellow, .red, .darkred]
    
    var body: some View {
        VStack(spacing: 0) {
            HStack (alignment: .top, spacing: 16) {
                VStack(alignment: .leading) {
                    ForEach(states, id: \.self) { color in
                        TrafficLightView(coronaState: color, selectedState: viewModel.state)
                    }
                }
                if let _ = viewModel.error {
                    VStack(alignment: .trailing, spacing: 16) {
                        Text(NSLocalizedString("Try to switch mode and move to a location within Germany to update the traffic light", comment: ""))
                            .multilineTextAlignment(.trailing)
                        Image("curved-arrow")
                            .resizable()
                            .frame(width: 64.0, height: 64.0)
                            .padding(.trailing, 6.0)
                    }
                    .frame(minWidth: 0,
                           maxWidth: .infinity,
                           minHeight: 0,
                           maxHeight: .infinity,
                           alignment: .bottomTrailing
                    )
                } else if viewModel.state == .unknown {
                    ProgressView()
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                } else {
                    VStack(alignment: .leading, spacing: 16) {
                        Text(NSLocalizedString("subtitle_\(viewModel.state.rawValue)", comment: ""))
                            .font(.subheadline)
                        ForEach((1...3), id: \.self) {
                            Text(NSLocalizedString("rule_\(viewModel.state.rawValue)_\($0)", comment: ""))
                                .font(.caption)
                        }
                    }
                    .frame(minWidth: 0,
                           maxWidth: .infinity,
                           minHeight: 0,
                           maxHeight: .infinity,
                           alignment: .top
                    )
                }
            }
            .padding()
            .frame(minHeight: 0,
                   maxHeight: 260)
            ZStack(alignment: .bottomTrailing) {
                MapView(centerCoordinate: $viewModel.centerCoordinate, showMapAlert: $showMapAlert, followUser: $viewModel.followUser, areas: viewModel.areas, color: viewModel.state.color)
                    .alert(isPresented: $showMapAlert) {
                        Alert(title: Text(NSLocalizedString("Location access denied", comment: "")),
                              message: Text(NSLocalizedString("Your location is needed", comment: "")),
                              primaryButton: .cancel(),
                              secondaryButton: .default(Text(NSLocalizedString("Settings", comment: "")),
                                                        action: { self.goToDeviceSettings() })
                        )
                    }
                    .onChange(of: viewModel.centerCoordinate, perform: { value in
                        viewModel.reload()
                    })
                    .edgesIgnoringSafeArea(.bottom)
                VStack() {
                    Button(action: { viewModel.followUser.toggle() }) {
                        Image(viewModel.followUser ? "pin" : "user")
                            .resizable()
                            .frame(width: 32.0, height: 32.0)
                            .padding()
                    }
                    .background(Color.white)
                    .cornerRadius(25)
                    .shadow(radius: 5)
                    .padding()
                    Spacer()
                }
                if viewModel.error == nil {
                    VStack(alignment: .leading) {
                        if let title = viewModel.item?.county {
                            Text(title)
                        }
                        if let subtitle = viewModel.item?.bl {
                            Text(subtitle)
                                .font(.caption)
                        }
                        if let cases7_bl_per_100k = viewModel.item?.cases7_bl_per_100k {
                            Text(String(format: NSLocalizedString("7 Tagesinzidenz/100.000 Einwohner %.2f", comment: ""), cases7_bl_per_100k))
                                .font(.caption)
                        }
                    }
                    .padding()
                    .frame(minWidth: 0,
                           maxWidth: .infinity,
                           alignment: .bottomLeading
                    )
                    .background(Color.white)
                    .cornerRadius(25)
                    .shadow(radius: 5)
                    .padding(.horizontal)
                    .padding(.bottom, 24)
                }
            }
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: .infinity
            )
        }
        .frame(minWidth: 0,
               maxWidth: .infinity,
               minHeight: 0,
               maxHeight: .infinity
        )
        
    }
}

extension HomeView {
    /// Path to device settings if location is disabled
    func goToDeviceSettings() {
        guard let url = URL.init(string: UIApplication.openSettingsURLString) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(viewModel: ItemViewModel(centerCoordinate: defaultCenterCoordinate))
    }
}
