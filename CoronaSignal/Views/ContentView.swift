//
//  ContentView.swift
//  CoronaSignal
//
//  Created by Hannes Tribus on 19/11/20.
//

import SwiftUI
import Combine

struct ContentView: View {
    let timer = Timer.publish(every: 10*60, on: .main, in: .common).autoconnect()
    @ObservedObject var viewModel: ItemViewModel = ItemViewModel()
    @ObservedObject var notificationManager = LocalNotificationService()
    
    var body: some View {
        view
    }
    
    @ViewBuilder
    private var view: some View {
        if viewModel.loading {
            ProgressView()
                .onAppear(perform: { viewModel.reload() })
                .frame(maxWidth: .infinity, maxHeight: .infinity)
        } else {
            NavigationView {
                HomeView(viewModel: viewModel)
                    .onReceive(timer) { time in
                        viewModel.reload()
                        if viewModel.dirty && viewModel.error == nil {
                            var body = [String]()
                            for n in 1...3 {
                                body.append(NSLocalizedString("rule_\(viewModel.state.rawValue)_\(n)", comment: ""))
                            }
                            notificationManager.sendNotification(title: NSLocalizedString("title_\(viewModel.state.rawValue)", comment: ""), subtitle: NSLocalizedString("subtitle_\(viewModel.state.rawValue)", comment: ""), body: body.joined(separator: "\n"), imageName: "traffic-light-\(viewModel.state.rawValue)")
                            viewModel.dirty = false
                        }
                    }
                    .navigationBarTitle(NSLocalizedString(viewModel.error != nil ? "No data at this location" : "title_\(viewModel.state.rawValue)", comment: ""), displayMode: .inline)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
