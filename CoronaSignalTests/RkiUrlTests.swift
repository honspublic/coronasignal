//
//  RkiUrlTests.swift
//  CoronaSignalTests
//
//  Created by Hannes Tribus on 19/11/20.
//

import XCTest
@testable import CoronaSignal

class RkiUrlTests: XCTestCase {

    var geometry: [Double] = []
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        geometry = [53.798, 10.111]
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testRkiUrlwithGeometry() throws {
        let rkiUrl = RkiUrl(geometry: geometry)
        XCTAssertNotNil(rkiUrl.url)
        if let url = rkiUrl.url {
            XCTAssertTrue(url.absoluteString.starts(with: "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_Landkreisdaten/FeatureServer/0/query?"))
            checkValue(original: queryStringParameter(url: url.absoluteString, param: kQueryGeometry), expected: "53.798,10.111")
            checkValue(original: queryStringParameter(url: url.absoluteString, param: kQueryOutFields), expected: "last_update,county,BL,cases7_bl_per_100k")
        } else {
            assertionFailure("Url is nil, even if checked before")
        }
    }
    
    func testRkiUrlwithGeometryAndOutFields() throws {
        let rkiUrl = RkiUrl(geometry: geometry, outFields: ["last_update"])
        XCTAssertNotNil(rkiUrl.url)
        if let url = rkiUrl.url {
            XCTAssertTrue(url.absoluteString.starts(with: "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_Landkreisdaten/FeatureServer/0/query?"))
            checkValue(original: queryStringParameter(url: url.absoluteString, param: kQueryGeometry), expected: "53.798,10.111")
            checkValue(original: queryStringParameter(url: url.absoluteString, param: kQueryOutFields), expected: "last_update")
        } else {
            assertionFailure("Url is nil, even if checked before")
        }
    }

    // MARK: - Private helpers
    
    fileprivate func checkValue(original: String?, expected: String) {
        XCTAssertNotNil(original)
        XCTAssertEqual(original, expected)
    }
    
    fileprivate func queryStringParameter(url: String, param: String) -> String? {
      guard let url = URLComponents(string: url) else { return nil }
      return url.queryItems?.first(where: { $0.name == param })?.value
    }
}
